(* Read/Write race *)
open Interface

let x = var "x" 0

let _ =
  let _ = spawn (fun () -> x := 1) in
  !x
