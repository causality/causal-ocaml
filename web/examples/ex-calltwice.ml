(* Higher-order: two calls *)
let _ = fun (f : unit -> unit) -> f () ; f ()
