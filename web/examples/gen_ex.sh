#! /bin/sh
TARGET="$1"
shift
(echo 'let list = [';
for i in "$@"; do
    TITLE=$(head -n 1 $i | sed -e 's/^(\* *//g' -e 's/ *\*)$//g')
    echo -n "\"$TITLE\",\""
    tail -n +2 $i | sed 's/;;//g' | sed 's/"/\\"/g'
    echo "\";"
done;
echo ']'
) > $TARGET
