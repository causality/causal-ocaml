(* List.map (Sequential) *)
open Interface

type list = [] | ( :: ) of int * list

(* Because of our parallel evaluation order, we have to force the evaluation
   order *)
let rec map f = function
  | [] -> []
  | t :: q ->
      let x = f t in
      x :: map f q
