(* Simple join *)
open Interface

let main (f, g) = (f () * 1, g () * 1)
