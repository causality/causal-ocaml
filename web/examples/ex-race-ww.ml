(* Write/Write race *)
open Interface

let x = var "x" 0

let _ = ((x := 1), (x := 2))

let _ = !x
