(* Message-Passing (Shared Memory) *)
open Interface

let flag = var "flag" 0

let data = var "data" 0

let sender () =
  data := 17 ;
  flag := 1

let receiver () = if !flag = 1 then Some !data else None

let _ = (sender (), receiver ())
