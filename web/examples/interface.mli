type var = {get: unit -> int; set: int -> unit}

val var : string -> int -> var

val spawn : (unit -> 'a) -> unit -> 'a

val ( ! ) : var -> int

val ( := ) : var -> int -> unit

val ( ||| ) : unit -> unit -> unit

val abort : unit -> 'a
