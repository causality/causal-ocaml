(* List.map (Parallel) *)
open Interface

type list = [] | ( :: ) of int * list

(* Because our evaluation is parallel, we get parallel map for free *)
let rec parmap f = function [] -> [] | t :: q -> f t :: parmap f q
