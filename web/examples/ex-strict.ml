(* Strictness testing *)
open Interface

let strict (f : 'a -> int) =
  let r = var "r" 0 in
  f (fun () -> r := 1) ;
  !r = 1
