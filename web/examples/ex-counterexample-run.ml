(* Counter-example to the full abstraction *)
open Interface

let r = var "r" 0

let s = var "s" 0

(* There is no OCaml f that can make [test f] converge. Can you find a
   context that produces a result? *)
let test (f : 'a -> unit) =
  f (fun () ->
      r := !r + 1 ;
      s := 1) ;
  r := !r + 1 ;
  if !s = 1 && !r = 1 then () else abort ()

let loc = create "loc"
let ignore _x = ()
let opponent g =
  ignore (spawn (fun () -> put loc 1));
  ignore (spawn (fun () -> put loc 2));
  let n = watch loc in
  if n = 1 then (g (); abort ())
  else ()
  
let _ = test opponent
