open Js_of_ocaml

type state =
  | Hidden
  | Moving of Html_types.div_content H.elt list
  | Stuck of Html_types.div_content H.elt list

let id = "tooltip"

let draw = function
  | Hidden -> H.set_class (H.get id) "hidden"
  | Moving content | Stuck content ->
      H.set_class (H.get id) "visible" ;
      H.Node.set_children (H.get id) content

let state = ref Hidden

let update new_state =
  state := new_state ;
  draw !state

let mousemove ev =
  match !state with
  | Moving _ ->
      let c = H.get id in
      c##.style##.top
      := Js.string @@ Printf.sprintf "%dpx" @@ (ev##.clientY + 5) ;
      c##.style##.left
      := Js.string @@ Printf.sprintf "%dpx" @@ (ev##.clientX + 5) ;
      Js._false
  | _ -> Js._false

let init () =
  update Hidden ;
  Dom_html.window##.onmousemove := Dom_html.handler mousemove

let set_move content =
  match !state with Stuck _ -> () | _ -> update (Moving content)

let stick () = match !state with Moving c -> update (Stuck c) | _ -> ()

let hide () = match !state with Moving _ -> update Hidden | _ -> ()
