open Js_of_ocaml
open Model

let i = ref 0

let string_input ?default validate prompt placeholder =
  let default_value =
    match default with None -> [] | Some x -> [H.a_value x]
  in
  let input =
    H.input ~a:(default_value @ [H.a_placeholder placeholder]) ()
  in
  let get () = validate @@ Js.to_string (H.Cast.of_input input)##.value in
  let html = [H.txt prompt; input] in
  (html, get)

let value_input prompt typ =
  string_input (Value.of_string Env.empty) prompt
  @@ Printf.sprintf "Value of type %s" (Ocaml.Typ.show typ)

let prefix l (h, f) = (l @ h, f)

let tuple l =
  let get () = List.map (fun (_, f) -> f ()) l in
  ( [ H.ul
        (List.mapi
           (fun n (h, _) ->
             H.li (H.txt (Printf.sprintf "Component #%d: " n) :: h))
           l) ]
  , get )

let record l =
  let get () = List.map (fun (name, (_, f)) -> (name, f ())) l in
  ( [ H.ul
        (List.map
           (fun (s, (h, _)) ->
             H.li (H.txt (Printf.sprintf "Field %s: " s) :: h))
           l) ]
  , get )

let const text c = ([H.txt text], fun () -> c)

let pat_i i = function
  | Some {Typedtree.pat_desc= Typedtree.Tpat_tuple l; _} ->
      Some (List.nth l i)
  | _ -> None

let pat_field' s = function
  | Some Typedtree.{pat_desc= Tpat_record (l, _); _} -> (
    try
      let _, _, c = List.find (fun (_, name, _) -> name = s) l in
      Some c
    with _ -> None )
  | _ -> None

let rec form_of_typ pat env typ =
  match (Ctype.repr typ).desc with
  | Types.Tarrow (_, src, tgt, _) ->
      let default =
        match pat with
        | Some Typedtree.{pat_desc= Tpat_var (s, _) | Tpat_alias (_, s, _); _}
          ->
            Some (Ident.name s)
        | _ -> None
      in
      string_input ?default
        (fun name ->
          ( Value.Function {src; tgt; name; data= (); pat= None}
          , Location.none ))
        (Printf.sprintf "Function (%s):" Ocaml.Typ.(show (src --> tgt)))
        "Function name"
  | Types.Ttuple l ->
      H.Form.map
        (fun l -> (Value.Tuple l, Location.none))
        (tuple (List.mapi (fun i ty -> form_of_typ (pat_i i pat) env ty) l))
  | Types.Tconstr (name, _, _) -> (
    match (Env.find_type name env).Types.type_kind with
    | Types.Type_record (fields, _) ->
        H.Form.map (fun l -> (Value.Record l, Location.none))
        @@ record
             (List.map
                (fun field ->
                  ( Ident.name field.Types.ld_id
                  , form_of_typ pat env field.Types.ld_type ))
                fields)
    | Types.Type_variant variants ->
        let make_choice variant =
          match variant.Types.cd_args with
          | Types.Cstr_tuple l ->
              ( Ident.name variant.Types.cd_id
              , lazy (tuple (List.map (form_of_typ pat env) l)) )
          | Types.Cstr_record l ->
              let make {Types.ld_id; ld_type; _} =
                (Ident.name ld_id, form_of_typ pat env ld_type)
              in
              ( Ident.name variant.Types.cd_id
              , lazy (H.Form.map (List.map snd) @@ record (List.map make l))
              )
        in
        let recombine (constr_name, args) =
          (Value.Constructor (constr_name, args), Location.none)
        in
        H.Form.map recombine
        @@ H.Form.combine "Enter a constructor"
             (List.map make_choice variants)
    | _ -> value_input "Enter value: " typ
    | exception Not_found ->
        Printf.printf "Type `%s` not found." (Path.name name) ;
        value_input "Enter value: " typ )
  | _ -> value_input "Enter value: " typ

let run_form (form_html, form_get) title submit_text k =
  match form_html with
  | [] -> ignore @@ k (form_get ())
  | _ ->
      let cont _ =
        k (form_get ()) ;
        false
        (* Ocaml.Term.handle (fun () -> k (form_get ())) ~on_error:(fun s _
           -> H.Node.set_children (H.get "form_error") [H.txt s]) ; false*)
      in
      H.set_class (H.get "form") "visible" ;
      H.Node.set_children (H.get "form")
        ( title @ form_html
        @ [ H.br ()
          ; H.div ~a:[H.a_id "form_error"] []
          ; H.br ()
          ; H.button cont submit_text
          ; H.button
              (fun _ ->
                H.set_class (H.get "form") "" ;
                false)
              "Cancel" ] )

let input pat env title submit_text typ k =
  run_form (form_of_typ pat env typ) title submit_text (fun x ->
      H.set_class (H.get "form") "" ;
      k x)
