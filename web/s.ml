(* A library of HTML widgets. *)
open Js_of_ocaml
open Js_of_ocaml_tyxml
include Js_of_ocaml_tyxml.Tyxml_js.Svg

module To_dom = Tyxml_cast.MakeTo (struct
  type 'a elt = 'a Tyxml_js.Svg.elt

  let elt = Tyxml_js.Svg.toelt
end)

module Of_dom = Tyxml_cast.MakeOf (struct
  type 'a elt = 'a Tyxml_js.Svg.elt

  let elt = Tyxml_js.Svg.tot
end)

let empty = Unsafe.data ""

let when_def x f = match x with None -> empty | Some a -> f a

let guard b e = if b then e else empty

let guard' b e = if b then e else []

let opt_get x = Js.Opt.get x (fun _ -> assert false)

module Node = struct
  let rec reset node =
    Js.Opt.case node##.firstChild
      (fun _ -> ())
      (fun c ->
        ignore @@ node##removeChild c ;
        reset node)

  let add_child (node : Dom.node Js.t) child =
    ignore @@ node##appendChild (To_dom.of_node child)

  let add_children node children = List.iter (add_child node) children

  let set_children (node : Dom.node Js.t) children =
    reset node ;
    add_children node children
end

let button description tit onclick =
  tspan
    ~a:[a_onclick (fun x -> onclick x ; true); a_class ["button"]]
    [txt description; title (txt tit)]

let concat sep l =
  let rec aux = function
    | [] -> []
    | [t] -> [t]
    | t :: q -> t :: sep () :: aux q
  in
  aux (List.filter (fun x -> x <> empty) l)

let concat' sep l =
  let rec aux = function
    | [] -> []
    | [t] -> [t]
    | t :: q -> t :: sep () :: aux q
  in
  List.concat @@ aux (List.filter (fun x -> x <> []) l)
