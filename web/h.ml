(* A library of HTML widgets. *)
open Js_of_ocaml
open Js_of_ocaml_tyxml.Tyxml_js
include Js_of_ocaml_tyxml.Tyxml_js.Html
module Cast = To_dom

module Node = struct
  let reset (x : Dom_html.element Js.t) = x##.innerHTML := Js.string ""

  let add_child (node : Dom_html.element Js.t) child =
    ignore @@ node##appendChild (Cast.of_node child)

  let add_children node children = List.iter (add_child node) children

  let set_children node children =
    reset node ;
    add_children node children

  let set_content node children =
    reset (Cast.of_element node) ;
    add_children (Cast.of_element node) children
end

let i = ref 0

let fix f =
  let rec fixpoint = lazy (f fixpoint) in
  Lazy.force fixpoint

let new_id () =
  incr i ;
  "_" ^ string_of_int !i

let empty = Unsafe.data ""

let when_def x f = match x with None -> empty | Some a -> f a

let guard b e = if b then e else empty

let guard' b e = if b then e else []

let concat sep l =
  let rec aux = function
    | [] -> []
    | [t] -> [t]
    | t :: q -> t :: sep () :: aux q
  in
  aux (List.filter (fun x -> x <> empty) l)

let concat' sep l =
  let rec aux = function
    | [] -> []
    | [t] -> [t]
    | t :: q -> t :: sep () :: aux q
  in
  List.concat @@ aux (List.filter (fun x -> x <> []) l)

let set_class x cl = x##.className := Js.string cl

let get id =
  try Dom_html.getElementById id
  with _ -> failwith @@ Printf.sprintf "Element `%s` not found" id

let value elem = Js.to_string elem##.value

let combobox default list =
  let make_option (name, _) = option (txt name) in
  let value self = Js.to_string @@ (Cast.of_select self)##.value in
  let on_change self _ =
    let v = value (Lazy.force self) in
    List.assoc v list () ; true
  in
  let options =
    option
      ~a:[a_value ""; a_disabled (); a_selected (); a_hidden ()]
      (txt default)
    :: List.map make_option list
  in
  let combobox =
    fix (fun self ->
        select ~a:[a_onchange (on_change self); a_required ()] options)
  in
  ( (combobox : Html_types.select elt :> [> Html_types.select] elt)
  , fun () -> value combobox )

module Draw = struct
  let find tagName node =
    let rec go sibling =
      Js.Opt.case sibling
        (fun _ -> None)
        (fun sibling ->
          if sibling##.nodeName = tagName then Some sibling
          else go sibling##.nextSibling)
    in
    go node##.firstChild

  let es ?edges ?attrs ?(onclick = fun _ -> false)
      ?(onhover = fun _ -> false) ?(onout = fun _ -> false) format es k =
    let open Causality in
    let open Model in
    let module ES = ES.Make (GameModel.Label) in
    let module Event = ES.Event in
    let id e = "_" ^ string_of_int (Event.id e) in
    let dot_source =
      Dot.to_string (ES.to_dot ?edges ?attrs GameModel.Show.to_string es)
    in
    Printf.printf "Dot:\n%s\n" dot_source ;
    let draw_dot_function = Js.Unsafe.variable "drawDot" in
    let after_drawn (element : Html_types.div_content elt) =
      let on_node event =
        try
          let elem = get (id event) in
          ( match find (Js.string "text") elem with
          | Some text -> S.Node.set_children text (format event)
          | None -> () ) ;
          elem##.onclick := Dom.handler (fun _ -> Js.bool @@ onclick event) ;
          elem##.onmouseover :=
            Dom.handler (fun _ -> Js.bool @@ onhover event) ;
          elem##.onmouseout := Dom.handler (fun _ -> Js.bool @@ onout event)
        with _ -> ()
      in
      k element ;
      List.iter on_node (ES.events es)
    in
    ( Js.Unsafe.(
        fun_call draw_dot_function
          [| inject @@ Js.string dot_source
           ; inject @@ Js.wrap_callback after_drawn |])
      : unit )
end

let button f s = a ~a:[a_onclick f; a_class ["button"]] [txt s]

module Form = struct
  type 'a t = Html_types.div_content elt list * (unit -> 'a)

  let map f (h, get) = (h, fun () -> f (get ()))

  let prefix l (h, get) = (l @ h, get)

  let list forms =
    let get () = List.map (fun (_, f) -> f ()) forms in
    let h = [ul (List.map (fun (x, _) -> li x) forms)] in
    (h, get)

  let combine prompt choices =
    let span = div [] in
    let make_option (name, form) =
      (name, fun () -> Node.set_content span (fst (Lazy.force form)))
    in
    match choices with
    | [(s, (form : 'a t Lazy.t))] ->
        let h, get = Lazy.force form in
        (h, fun () -> (s, get ()))
    | _ ->
        let h, get = combobox prompt (List.map make_option choices) in
        ( [(h :> Html_types.div_content elt); span]
        , fun () ->
            let s = get () in
            (s, snd (Lazy.force @@ List.assoc s choices) ()) )
end
