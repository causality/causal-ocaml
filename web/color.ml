(* Code coloring *)
open Js_of_ocaml

let ( !! ) = Regexp.matched_string

type token = String of string | Class of string * token list

type command = Regexp of string * string | Loc of Location.t * string

let rec to_xml = function
  | String s -> H.txt s
  | Class (s, toks) -> H.(span ~a:[a_class [s]] (to_xml_list toks))

and to_xml_list l = List.map to_xml l

let exec_command_string string = function
  | Regexp (regexp, cl) ->
      let re = Regexp.regexp regexp in
      let rec aux acc i =
        match Regexp.search_forward re string i with
        | Some (pos, result) ->
            let tok = Class (cl, [String !!result]) in
            let acc =
              if pos > i then
                tok :: String (String.sub string i (pos - i)) :: acc
              else tok :: acc
            in
            aux acc (pos + (String.length @@ Regexp.matched_string result))
        | None ->
            List.rev
              (String (String.sub string i (String.length string - i)) :: acc)
      in
      aux [] 0
  | Loc (loc, cl) ->
      let sub = String.sub string in
      let i = loc.Location.loc_start.Lexing.pos_cnum
      and j = loc.Location.loc_end.Lexing.pos_cnum in
      if 0 <= i && 0 <= j then
        [ String (sub 0 i)
        ; Class (cl, [String (sub i (j - i))])
        ; String (sub j (String.length string - j)) ]
      else [String string]

let rec exec_command_token command = function
  | String s -> exec_command_string s command
  | Class (s, toks) ->
      [Class (s, List.concat @@ exec_command_tokens command toks)]

and exec_command_tokens command = List.map (exec_command_token command)

let rec exec_commands_token token = function
  | [] -> [token]
  | command :: q ->
      let tokens = exec_command_token command token in
      exec_commands_tokens tokens q

and exec_commands_tokens tokens commands =
  List.concat
  @@ List.map (fun tok -> exec_commands_token tok commands) tokens

let exec commands string =
  to_xml_list @@ exec_commands_token (String string) commands

let color ?(cl = []) ?(locs = []) string =
  H.div ~a:[H.a_class ("code" :: cl)]
  @@ exec
       ( List.map (fun (loc, cl) -> Loc (loc, cl)) locs
       @ [ Regexp
             ("let|match|with| rec |type|begin|end|if|then|else", "keyword")
         ; Regexp ("[A-Z][a-zA-Z_]*", "constr")
         ; Regexp ("\".*\"", "string")
         ; Regexp ("[0-9]+", "number") ] )
       string
