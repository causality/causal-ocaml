let ghost = Location.none

open Js_of_ocaml

let get_input () =
  try Js.to_string (Obj.magic (H.get "input"))##.value with _ -> ""

let set_input s = (Obj.magic (H.get "input"))##.value := Js.string s

let set_error error =
  let container = H.get "error" in
  if error = [] then H.set_class container ""
  else (
    H.set_class container "visible" ;
    H.(Node.set_children container error) )

let run_computation f x =
  let _ = set_error [] in
  let input = get_input () in
  Model.Ocaml.Term.handle
    (fun () -> f x)
    ~on_error:(fun s loc ->
      match loc with
      | None -> set_error [H.txt s]
      | Some loc ->
          set_error
            [ H.txt s
            ; Color.color ~cl:["indented"] ~locs:[(loc, "hl-error")] input ])

let reset () =
  set_error [] ;
  H.Node.set_children (H.get "form") []

open Model
open Causality
module Monad = GameModel.Monad (Monad)
module Interpreter = Model.Interpreter.Make (Monad)
module ES = ES.Make (GameModel.Label)
module Event = ES.Event
module GS = Model.GameSemantics.Make (Monad)
module B = Model.Builtins.Make (Monad)

let emit_neutral loc str =
  let open Monad in
  (* get_global_world >>= fun world -> Printf.printf "Emitting neutral event
     `%s' in world %s" str (World.show world) ;*)
  neutral GameModel.Signature.Player.Program {str; loc} >> return ()

type state =
  | Edit of string
  | View of (string * Env.t * unit Monad.ComputationState.t)

let state = ref (Edit "let f = succ")

let redraw_ = ref (fun () -> ())

let redraw () = !redraw_ ()

let edit_code s _ =
  state := Edit s ;
  redraw () ;
  true

let highlight location =
  match !state with
  | View (s, _, _) ->
      H.Node.set_children (H.get "code")
        [Color.color ~locs:[(location, "hl-error")] s]
  | _ -> ()

let remove_highlight () =
  match !state with
  | View (s, _, _) ->
      H.Node.set_children (H.get "code") [Color.color ~locs:[] s]
  | _ -> ()

let r = ref 0

let run_program s =
  let module Cell = Linearisable.Cell (Monad) in
  let exp, env = Model.Ocaml.Term.structure B.init s in
  ( env
  , Monad.ComputationState.(
      run (init ())
        Monad.(
          Interpreter.structure (B.builtins emit_neutral) [] exp
          >>= GS.send GameModel.Signature.Player.Program)) )

let compute_interp _ =
  let s = get_input () in
  let env, cstate = run_program s in
  state := View (s, env, cstate) ;
  redraw ()

let feed_msg history world ?pointer address kind payload =
  match !state with
  | Edit _ -> ()
  | View (s, env, cstate) ->
      let query view =
        let view = Option.value view ~default:Event.View.empty in
        ( Some
            (List.fold_left
               (fun view ev ->
                 Option.get @@ Event.View.join view (Event.prime_view ev))
               view history)
        , () )
      in
      let world, _ = World.query Monad.view query world in
      let cstate' =
        Monad.ComputationState.run ~world cstate
          Monad.(
            play ?pointer GameModel.Signature.Player.Context address
              {kind; payload}
            >> return ())
      in
      state := View (s, env, cstate') ;
      redraw ()

let return_from_context_closure env event id typ move _ =
  run_computation
    (fun () ->
      InputValue.input None env
        [ H.h1
            [ H.txt
              @@ Printf.sprintf "Enter return value (type: %s)"
                   Model.Ocaml.Typ.(show typ) ] ]
        "Return this value" typ
        (feed_msg [event] (Event.world event) ~pointer:move
           [Address.Result id] Return))
    ()

let call_program_closure env event {Model.Value.src; tgt; pat; _} move
    address _ =
  run_computation
    (fun () ->
      InputValue.input pat env
        [ H.h1
            [ H.txt
              @@ Printf.sprintf "Enter argument value (type: %s)"
                   Model.Ocaml.Typ.(show src) ] ]
        "Call the function with this argument" src
        (feed_msg [event] (Event.world event) ~pointer:move address
           (Call tgt)))
    ()

let format_move env e =
  GameModel.Show.to_document S.empty S.txt S.button
    (return_from_context_closure env e)
    (call_program_closure env e)
    (Event.label e)

let loc_of x =
  match (Event.label x).GameModel.Label.desc with
  | `Neutral {GameModel.Signature.loc; _} -> Some loc
  | `Visible {GameModel.Label.visible= {payload= _, loc; _}; _} -> Some loc

let es_of_computation_state env state k =
  let es = Monad.project @@ state in
  let pointers =
    List.filter_map
      (fun evt ->
        match (Event.label evt).GameModel.Label.desc with
        | `Visible {GameModel.Label.pointer= None; _} | `Neutral _ -> None
        | `Visible {GameModel.Label.pointer= Some k; _} ->
            Some
              (Dot.Edge.make
                 ~src:("_" ^ string_of_int k.id)
                 ~tgt:("_" ^ string_of_int (Event.id evt))
                 ~attrs:[Dot.Attribute.style "dotted"]))
      (ES.events es)
  in
  H.Draw.es ~edges:pointers
    ~attrs:(fun evt ->
      let cl, color =
        match (Event.label evt).GameModel.Label.desc with
        | `Neutral _ -> ("neutral", "")
        | _ when GameModel.Label.player (Event.label evt) = Program ->
            ("player", "#00ff00")
        | _ -> ("context", "#ff0000")
      in
      [ Dot.Attribute.class_ cl
      ; Dot.Attribute.style "filled"
      ; Dot.Attribute.shape "rectangle"
      ; Dot.Attribute.fillcolor color ])
    ~onhover:(fun evt ->
      match loc_of evt with Some loc -> highlight loc ; false | _ -> false)
    ~onout:(fun _ -> Tooltip.hide () ; remove_highlight () ; false)
    (format_move env) es k

let rec draw_state k = function
  | Edit s ->
      k
        ( [ H.(textarea ~a:[a_id "input"] (txt s))
          ; H.span
              [ fst
                @@ H.combobox "Load an example"
                     (List.map
                        (fun (s, s') -> (s, fun () -> set_input s'))
                        Exs.list)
              ; H.button
                  (fun _ ->
                    run_computation compute_interp () ;
                    false)
                  "Compute the interpretation" ] ]
        , "edit" )
  | View (s, env, state) ->
      es_of_computation_state env state (fun content ->
          k
            ( H.
                [ div ~a:[a_id "left"]
                    [ h2 [txt "Source"]
                    ; div
                        ~a:[a_id "code"; a_onclick (edit_code s)]
                        [Color.color s]
                    ; H.button (edit_code s) "Edit the program" ]
                ; div ~a:[a_id "right"] [H.h2 [txt "Interpretation"]; content]
                ]
            , "view" ))

and redraw () =
  reset () ;
  draw_state
    (fun (kids, cl) ->
      H.(set_class (get "root")) cl ;
      H.Node.set_children (H.get "root") kids)
    !state

let init _ =
  Tooltip.init () ;
  (redraw_ :=
     fun () ->
       draw_state
         (fun (kids, cl) ->
           H.(set_class (get "root")) cl ;
           H.Node.set_children (H.get "root") kids)
         !state) ;
  redraw () ; Js._true

let () = Dom_html.window##.onload := Dom_html.handler init
