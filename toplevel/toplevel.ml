module Monad = Model.GameModel.Monad (Causality.Monad)
module I = Model.Interpreter.Make (Monad)
module B = Model.Builtins.Make (Monad)
module ES = Causality.ES.Make (Model.GameModel.Label)
module GS = Model.GameSemantics.Make (Monad)
open Monad

let ( let* ) = bind

let run_source source =
  let str, _ = Model.Ocaml.Term.structure B.init source in
  let program str =
    let* value = I.structure (B.builtins (fun _ _ -> return ())) [] str in
    Printf.printf "Result: %s\n%!" (I.SValue.to_string value) ;
    GS.send Model.GameModel.Signature.Player.Program value
  in
  let result = Monad.ComputationState.(run (init ()) (program str)) in
  let show x =
    let open Model.GameModel.Label in
    match x.desc with `Neutral _ -> "neutral" | `Visible _ -> "visible"
  in
  let dot = ES.to_dot show (Monad.project result) in
  Causality.Dot.view ~viewer:"evince" dot

let read_in inp =
  let rec aux s =
    match input_line inp with
    | exception End_of_file -> s
    | line -> aux (s ^ "\n" ^ line)
  in
  aux ""

let () =
  let with_input f =
    if Array.length Sys.argv <= 1 then f stdin
    else
      let fd = open_in Sys.argv.(1) in
      let v = f fd in
      close_in fd ; v
  in
  Model.Ocaml.Term.handle
    ~on_error:(fun err _ -> prerr_endline err)
    (fun () -> with_input (fun inp -> run_source @@ read_in inp))
