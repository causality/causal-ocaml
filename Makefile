view:
	dune build
	firefox _build/default/web/index.html

web:
	$(PANDOC) index.md > $(PUBLISH_DIR)/index.html
	dune build --profile release
	dune build @doc
	cp -r _build/default/_doc/_html/ $(PUBLISH_DIR)/doc
	cp _build/default/web/*css _build/default/web/*js _build/default/web/*opaque _build/default/web/index.html $(PUBLISH_DIR)

.PHONY: web



