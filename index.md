% Causal OCaml

Causal OCaml is an implementation of a causal and interactive
semantics for a "concurrent" subset of OCaml. Given an OCaml program
as input, it computes an event structure describing its behaviour.

- [demo](demo.html)
- [source code](https://gitlab.inria.fr/causality/causal-ocaml)
- [documentation of the source code](doc/)

# The source language

We describe the language accepted by OCaml. It is a subset of OCaml:
we `compiler-libs` which means that the parser and the typer are the
same.

## Features of OCaml

Causal OCaml supports all the basic functional aspects of OCaml, which means:

- Product types, records, sum types including recursive types
- Functions (no labelled or optional arguments) including recursive functions

In particular, what is *not* supported:

- Any kind of module system
- Exceptions
- Polymorphic variant

Some things are sort of working "by chance"

- Some GADT, being simple variants may work
- Some degree of polymorphism is allowed even though the web interface
  struggles to input polymorphic values and no further unification is
  done.

## Concurrent semantics
To showcase the causal model, we have given a concurrent semantics to this traditional subset. This means that everything that can be executed in parallel will, in particular:

- Arguments to a function
- Components of a pair

## Stdlib

The standard library of OCaml is not available. Instead a mini stdlib
is available containing the following operations:

```ocaml
val ( + ) : int -> int -> int
val ( - ) : int -> int -> int
val ( * ) : int -> int -> int
val ( / ) : int -> int -> int
val ( <= ) : int -> int -> bool
val ( < ) : int -> int -> bool
val ( = ) : int -> int -> bool
val ( > ) : int -> int -> bool
val ( >= ) : int -> int -> bool
val ( && ) : bool -> bool -> bool
val ( || ) : bool -> bool -> bool
val succ : int -> int
val spawn : (unit -> unit) -> (unit -> unit)
val abort : unit -> 'a
type var
val var : string -> int -> var
val ref : int -> var
val ( ! ) : var -> int
val ( := ) : var -> int -> unit"
```
This should be fairly standard, except:

- `abort` terminates the current thread but not the whole execution
- var is the type of concurrent references
- The first argument to var is an optional variable name to be displayed in the write/read events on this var
- `ref` is just `var ""`
- `spawn f` spawns `f` in the background and return a waiter function
  `g`, which when called waits for that call of `f` to be finished.

# How to use
The interface lets you input a piece of code to be interpreted inside
the model. The whole event structure corresponding to the program is
not computed right away (as it is often infinite). Rather, the
interface lets you explore it interactively. The first event is always
a Return from Program (in green) which is the return value of the code
you have entered. If this is a function, then this will just say the
input/output type of the function and lets you interact with it. To
interact with it, you can click on the function type which ask you for
a value of the input type and simulate a call to the function and
generate a red (Context: you) call event. Once again, the value is not
a value in the OCaml sense, but rather an interactive value where
functions are merely endpoints for interaction. If passing a function
to Program (eg. if the program has a higher-order type).

# A few words on the implementation.
