module Make (Monad : Monad.T) = struct
  open Monad
  open Typedtree

  let ( >>= ) = Monad.bind

  let ( >|= ) m f = m >>= fun x -> return (f x)

  module SValue = MonadicValue.Make (Monad)

  exception Unsupported of string * Warnings.loc

  let rec reduce f acc = function
    | [] -> return acc
    | t :: q -> f acc t >>= fun x -> reduce f x q

  let fail ?loc msg =
    raise (Unsupported (msg, Option.value ~default:Location.none loc))

  type env_value = Immediate of SValue.t | Indirect of (unit -> SValue.t t)

  let closure_num = ref 0

  let lookup env name =
    match List.assoc name env with
    | Immediate v -> return v
    | Indirect f -> f ()

  let rec do_match : type a. a general_pattern -> _ = fun pattern value ->
    let fail = fail ~loc:pattern.pat_loc in
    let loc = snd value in
    match pattern.pat_desc with
    | Tpat_value v -> do_match (v :> value general_pattern) value
    | Tpat_any -> Some ([], value)
    | Tpat_alias (pat, ident, _) -> (
      match do_match pat value with
      | Some (env, v) -> Some ((Ident.name ident, Immediate value) :: env, v)
      | _ -> None )
    | Tpat_var (s, _) -> (
      match value with
      | Value.Function c, _ ->
          Some
            ( [(Ident.name s, Immediate value)]
            , (Value.Function {c with Value.name= Ident.name s}, loc) )
      | _ -> Some ([(Ident.name s, Immediate value)], value) )
    | Tpat_tuple l -> (
      match fst value with
      | Value.Tuple l' -> (
        match do_match_list l l' with
        | Some (env, vs) -> Some (env, (Value.Tuple vs, loc))
        | None -> None )
      | _ -> None )
    | Tpat_constant c ->
        if fst value = Const c then Some ([], value) else None
    | Tpat_construct (_, {cstr_name; _}, args) -> (
      match fst value with
      | Value.Constructor (s, args') ->
          if s = cstr_name then
            match do_match_list args args' with
            | Some (env, args') ->
                Some (env, (Value.Constructor (s, args'), loc))
            | None -> None
          else None
      | _ -> None )
    | Tpat_record (list, _) -> (
      match fst value with
      | Value.Record fields ->
          Option.map (fun (env, fields) -> (env, (Value.Record fields, loc)))
          @@ do_match_record list fields
      | _ -> None )
    | _ -> fail "Unsuppported pattern"

  and do_match_list l l' =
    try
      let envs, args = List.(split @@ map Option.get (map2 do_match l l')) in
      Some (List.concat envs, args)
    with _ -> None

  and do_match_record list fields =
    let m (_, {Types.lbl_name; _}, pat) =
      Option.map (fun (env, v) -> (env, (lbl_name, v)))
      @@ do_match pat (List.assoc lbl_name fields)
    in
    try
      let env, records =
        List.(split @@ List.map Option.get (List.map m list))
      in
      Some (List.concat env, records)
    with _ -> None

  let rec do_case : type a. a case -> _ = fun case builtins env v ->
    match do_match case.c_lhs v with
    | None -> return None
    | Some (env', _) -> (
        let env = env' @ env in
        ( match case.c_guard with
        | None -> return Value.ttrue
        | Some e -> do_transl_exp builtins env' e >|= fst )
        >>= function
        | Value.Constructor ("true", []) ->
            do_transl_exp builtins env case.c_rhs
            >>= fun x -> return (Some x)
        | Value.Constructor ("false", []) -> return None
        | _ ->
            Printf.eprintf "Guard has not returned a boolean" ;
            return None )

  and do_cases : type a. a case list -> _ = fun li builtins env v -> match li with
    | [] -> Monad.discard
    | c :: cs -> (
        do_case c builtins env v
        >>= function
        | Some v -> return v | None -> do_cases cs builtins env v )

  and do_transl_exp builtins env exp =
    let loc = exp.exp_loc in
    let fail = fail ~loc in
    let typ = exp.exp_type in
    match exp.exp_desc with
    | Texp_constant c -> return @@ (Value.Const c, loc)
    | Texp_ident (_, {txt= Lident s; _}, _) -> (
      try lookup env s
      with Not_found -> (
        try return (List.assoc s builtins (), loc)
        with _ -> fail (Printf.sprintf "Unknown builtin: %s" s) ) )
    | Texp_let (rec_flag, bindings, e) ->
        do_bindings rec_flag builtins env bindings
        >>= fun (_, env) -> do_transl_exp builtins env e
    | Texp_sequence (t, u) ->
        do_transl_exp builtins env t >>= fun _ -> do_transl_exp builtins env u
    | Texp_match (exp, cases, _) ->
        do_transl_exp builtins env exp
        >>= fun v -> do_cases cases builtins env v
    | Texp_construct (_, descr, args) ->
        join_list @@ List.map (do_transl_exp builtins env) args
        >>= fun vs -> return @@ (Value.Constructor (descr.cstr_name, vs), loc)
    | Texp_apply (func, args) ->
        let args =
          try List.map (fun (_, x) -> Option.get x) args
          with _ ->
            fail
              "Optional arguments or labelled arguments are not supported."
        in
        do_application builtins env func args loc
    | Texp_tuple [e] -> do_transl_exp builtins env e
    | Texp_tuple l ->
        join_list (List.map (do_transl_exp builtins env) l)
        >>= fun l -> return @@ (Value.Tuple l, loc)
    | Texp_record {extended_expression; fields; _} ->
        let aux record ({Types.lbl_name; _}, x) =
          match x with
          | Overridden (_, exp) ->
              do_transl_exp builtins env exp
              >>= fun v ->
              return @@ ((lbl_name, v) :: List.remove_assoc lbl_name record)
          | _ -> return record
        in
        ( match extended_expression with
        | Some e -> (
            do_transl_exp builtins env e
            >>= function
            | Value.Record l, _ -> return l | _ -> fail "Expected record" )
        | None -> return [] )
        >>= fun record ->
        reduce aux record (Array.to_list fields)
        >>= fun l -> return @@ (Value.Record l, loc)
    | Texp_field (e, _, field) -> (
        do_transl_exp builtins env e
        >>= function
        | Value.Record l, _ -> return (List.assoc field.lbl_name l)
        | _ -> discard )
    | Texp_function f -> (
      match Ocaml.Typ.decompose_arrow typ with
      | None -> fail ""
      | Some (source, target) ->
          let pat = (List.hd f.cases).c_lhs in
          let clos _ v = do_cases f.cases builtins env v in
          let name =
            Printf.sprintf "anonymous#%d" (incr closure_num ; !closure_num)
          in
          return
            (SValue._function ~name ~pat ~src:source ~tgt:target clos, loc) )
    | Texp_ifthenelse (a, b, c) -> (
        do_transl_exp builtins env a
        >>= function
        | Value.Constructor ("true", _), _ -> do_transl_exp builtins env b
        | _ -> (
          match c with
          | Some e -> do_transl_exp builtins env e
          | _ -> return (Value.unit, loc) ) )
    | _ -> return (Value.unit, loc)

  and do_bindings rec_flag builtins env bindings =
    let rec aux acc env = function
      | [] -> return (List.rev acc, env)
      | binding :: rest ->
          do_binding builtins env binding
          >>= fun (v, env) -> aux (v :: acc) env rest
    in
    match rec_flag with
    | Asttypes.Nonrecursive -> aux [] env bindings
    | Asttypes.Recursive ->
        let rec rec_env = lazy (List.map make_delayed_binding bindings @ env)
        and make_delayed_binding binding =
          match binding.vb_pat.pat_desc with
          | Tpat_var (s, _) ->
              ( Ident.name s
              , Indirect
                  (fun () ->
                    do_transl_exp builtins (Lazy.force rec_env)
                      binding.vb_expr) )
          | _ ->
              fail
                "In let rec: only simple patterns of a single variable are \
                 supported"
        in
        aux [] (Lazy.force rec_env) bindings

  and do_binding builtins env binding =
    let loc = binding.vb_loc in
    do_transl_exp builtins env binding.vb_expr
    >>= fun v ->
    let v =
      match (binding.vb_pat.pat_desc, v) with
      | Tpat_var (s, _), (Value.Function c, _) ->
          (Value.Function {c with name= Ident.name s}, loc)
      | _ -> (fst v, loc)
    in
    match do_match binding.vb_pat v with
    | None -> discard
    | Some (env', v) -> return (v, env' @ env)

  and do_application builtins env func args loc =
    let rec reduce = function
      | [] -> failwith "Empty application"
      | [(x, _)] -> return (x, loc)
      | v :: value :: rest -> (
        match v with
        | Value.Function {Value.data= SValue.Func exec; _}, _ ->
            exec loc value >>= fun v -> reduce (v :: rest)
        | _ ->
            failwith
            @@ Printf.sprintf "Non-functional application: %s"
                 (SValue.to_string v) )
    in
    join_list (List.map (do_transl_exp builtins env) (func :: args))
    >>= reduce

  let do_transl_struct_item builtins env str =
    match str.str_desc with
    | Tstr_type (_, _) -> return ((Value.unit, str.str_loc), env)
    | Tstr_value (rec_flag, bindings) -> (
        do_bindings rec_flag builtins env bindings
        >>= fun (vs, env) ->
        match vs with
        | [v] -> return (v, env)
        | _ -> return ((Value.Tuple vs, str.str_loc), env) )
    | Tstr_eval (e, _) ->
        do_transl_exp builtins env e >>= fun v -> return (v, env)
    | _ -> fail "Unsupported structure item"

  let rec do_transl_struct_items builtins env = function
    | [] -> return (Value.unit, Location.none)
    | [x] -> do_transl_struct_item builtins env x >|= fst
    | t :: q ->
        do_transl_struct_item builtins env t
        >>= fun (_, env) -> do_transl_struct_items builtins env q

  let do_transl_structure builtins env x =
    do_transl_struct_items builtins env x.str_items

  let inject = List.map (fun (name, v) -> (name, Immediate v))

  let expression builtins env = do_transl_exp builtins (inject env)

  let structure builtins env = do_transl_structure builtins (inject env)
end
