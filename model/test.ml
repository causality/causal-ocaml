let ghost = Location.none

open Causality

let i = ref 0

module Monad = GameModel.Monad (Monad)
module SemValue = MonadicValue.Make (Monad)
module Interpreter = Interpreter.Make (Monad)
module Builtins = Builtins.Make (Monad)
module GS = GameSemantics.Make (Monad)

let run_value v =
  match Monad.(run v) with
  | [v], _ -> print_endline (SemValue.to_string v)
  | _ -> ()

let run_env v =
  match Monad.(run v) with
  | [v], _ ->
      print_endline
        ( String.concat " | "
        @@ List.map (fun (s, v) -> s ^ ": " ^ SemValue.to_string v) v )
  | _ -> ()

let _t v =
  run_value
    Monad.(parallel [v >>= GS.send Program >> discard; GS.receive Context])

let t v = _t (Monad.return (v, ghost))

let%expect_test _ =
  t (Value.int 3) ;
  [%expect {| 3 |}]

let%expect_test _ =
  t Value.(Tuple [(Value.int 3, ghost); (Value.string "foo", ghost)]) ;
  [%expect {| (3, "foo") |}]

let%expect_test _ =
  t
    Value.(
      Record [("x", (Value.int 3, ghost)); ("y", (Value.string "foo", ghost))]) ;
  [%expect {| {x=3; y="foo"} |}]

let value_of_string ?(env = []) s =
  let tree = Ocaml.Term.expression Builtins.init s in
  Interpreter.expression
    (Builtins.builtins (fun _ _ -> Monad.return ()))
    env tree

let%expect_test _ =
  _t (value_of_string "3") ;
  [%expect {| 3 |}]

let%expect_test _ =
  _t (value_of_string "(1, 2, 3)") ;
  [%expect {| (1, 2, 3) |}]

let%expect_test _ =
  _t (value_of_string "3+3") ;
  [%expect {| 6 |}]

(*let test_env_1 env = let g = Location.create () in let open Monad in let
  computation = join_list (List.map (fun (s, s') -> value_of_string s' >|=
  fun v -> (s, v)) env) >>= fun env -> parallel [GS.send_env g env >>
  discard; GS.receive_env g >|= fst] in run_env computation

  let%expect_test _ = test_env_1 [("x", "3"); ("y", "(1, 2, 3)"); ("z",
  "{x=1;y=\"foo\"}")] ; [%expect {| x: 3 | y: (1, 2, 3) | z: {y="foo"; x=1}
  |}]*)

(*let show_es c = ES.Monad.(get_es c)

  let test_interpret s = let computation = Monad.( Interpreter.structure
  (Builtins.builtins (fun _ _ -> Monad.return ())) [] (fst @@
  Ocaml.Term.structure Builtins.init s) >>= GS.send Program g) in show_es
  computation

  let test_call f arg = let g = GS.create "game" in let c = Monad.( parallel
  [ value_of_string f >>= GS.send Program g >> discard ; ( GS.receive Context
  g >>= fun f -> value_of_string arg >>= fun arg -> SemValue.apply ~arg f )
  ]) in show_es c

  let%expect_test _ = test_call "(fun x -> x + 1)" "3" ; [%expect {| digraph
  es { _7 [id="_7", label="{Return({Function}) @ game(.)}"] _8 [id="_8",
  label="{Call(3) @ game(. -> .)}"] _9 [id="_9", label="{Return(4) @ game(.
  -> . -> .result(8))}"] _7 -> _8 [arrowhead="empty"] _8 -> _9
  [arrowhead="empty"] } |}]

  let%expect_test _ = test_call "(fun f -> f (f 2))" "(fun n -> 3 * n)" ;
  [%expect {| digraph es { _10 [id="_10", label="{Return({Function}) @
  game(.)}"] _11 [id="_11", label="{Call({Function}) @ game(. -> .)}"] _12
  [id="_12", label="{Call(2) @ game(. -> . -> .arg(11))}"] _13 [id="_13",
  label="{Return(6) @ game(. -> . -> .arg(11) -> .result(12))}"] _14
  [id="_14", label="{Call(6) @ game(. -> . -> .arg(11))}"] _15 [id="_15",
  label="{Return(18) @ game(. -> . -> .arg(11) -> .result(14))}"] _16
  [id="_16", label="{Return(18) @ game(. -> . -> .result(11))}"] _10 -> _11
  [arrowhead="empty"] _11 -> _12 [arrowhead="empty"] _12 -> _13
  [arrowhead="empty"] _11 -> _14 [arrowhead="empty"] _13 -> _14
  [arrowhead="empty"] _14 -> _15 [arrowhead="empty"] _15 -> _16
  [arrowhead="empty"] } |}]

  let%expect_test _ = test_call "(fun x -> !x)" "(ref 0)" ; [%expect {|
  digraph es { _17 [id="_17", label="{Return({Function}) @ game(.)}"] _18
  [id="_18", label="{Call((\"\", ({Function}, {Function}))) @ game(. -> .)}"]
  _19 [id="_19", label="{Call(()) @ game(. -> . -> .arg(18).(1).(1))}"] _20
  [id="_20", label="{[get@:--->] @ ([])}"] _21 [id="_21", label="{Return(0) @
  game(. -> . -> .arg(18).(1).(1) -> .result(19))}"] _22 [id="_22",
  label="{Return(0) @ game(. -> . -> .result(18))}"] _17 -> _18
  [arrowhead="empty"] _18 -> _19 [arrowhead="empty"] _19 -> _20
  [arrowhead="empty"] _20 -> _21 [arrowhead="empty"] _21 -> _22
  [arrowhead="empty"] } |}]

  let%expect_test _ = test_interpret "let flag = var \"flag\" 0\n\ \ let data
  = var \"data\" 0\n\ \ let sender () =\n\ \ data := 17 ;\n\ \ flag := 1\n\n\
  let receiver () = if !flag = 1 then Some !data else None\n\ \ let _ =
  (sender (), receiver ())" *)
