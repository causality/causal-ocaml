(** Path in a value/type *)

(** In this module, we define paths in sum/records type. A path points to a
    subtype of a OCaml type.

    A path is a list of selector, which say how to locally orient in a type. *)

(** A selector is a pointer to an immediate subtype of a type *)
type selector =
  | Field of string
      (** [Field s] points to the type of field [s] of a record type *)
  | Projection of int
      (** [Projection i] points to the ith component of a tuple type *)
  | Argument of int
      (** [Argument n] points to the argument type of a function type. The
          integer represents the ID of the call. In game semantics term, this
          is the copy index of the call. *)
  | Result of int
      (** [Result n] points to the result type of a function type. As for
          Argument, the integer represents the ID of the Call. *)

val show_atom : selector -> string

(** An address in a type is a list of selector (representing a branch in the
    AST of the type. *)
type t = selector list

val show : t -> string

val at : 'closure Value.t -> t -> 'closure Value.t option
(** [at v path] returns the subvalue of [v] projected along [path]. This is
    partial as the [v] might not match [path]. *)

val map :
     const:(t -> Asttypes.constant -> Warnings.loc -> 'a)
  -> tuple:(t -> 'a list -> Warnings.loc -> 'a)
  -> record:(t -> 'b list -> Warnings.loc -> 'a)
  -> record_item:(t -> string -> 'a -> 'b)
  -> constructor:(t -> string -> 'a list -> Warnings.loc -> 'a)
  -> func:(t -> 'c Value.func -> Warnings.loc -> 'a)
  -> t
  -> 'c Value.t
  -> 'a

val map_value :
  (t -> 'a Value.func -> Warnings.loc -> 'b) -> t -> 'a Value.t -> 'b Value.t

val compare : t -> t -> int
