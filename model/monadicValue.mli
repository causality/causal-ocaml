(** Monadic values - values where functions are kleisli maps for a certain
    monad. *)
module Make (Monad : Monad.T) : sig
  type data = Func of (Warnings.loc -> t -> t Monad.t)

  and desc = data Value.desc

  and t = data Value.t

  type venv = (string * t) list

  type benv = (string * (unit -> desc)) list

  val show_function : 'a Value.func -> string

  val to_string : 'a Value.t -> string

  val to_string_kind : 'a Value.desc -> string

  val apply : ?loc:Warnings.loc -> arg:t -> data Value.desc * 'a -> t Monad.t

  val _function :
       src:Types.type_expr
    -> tgt:Types.type_expr
    -> ?name:string
    -> ?pat:Ocaml.Pattern.t
    -> (Warnings.loc -> t -> t Monad.t)
    -> data Value.desc

  module Typ : sig
    module T : sig
      type 'a conv =
        { ocaml: Types.type_expr
        ; from_val: data Value.t -> 'a option
        ; to_val: 'a -> data Value.desc }

      val int : int conv

      val string : string conv

      val unit : unit conv

      val bool : bool conv

      val ( *** ) : 'a conv -> 'b conv -> ('a * 'b) conv

      val conv : 'a conv -> 'a -> data Value.desc
    end

    type 'a conv = 'a T.conv =
      { ocaml: Types.type_expr
      ; from_val: data Value.t -> 'a option
      ; to_val: 'a -> data Value.desc }

    val int : int conv

    val string : string conv

    val unit : unit conv

    val bool : bool conv

    val ( *** ) : 'a conv -> 'b conv -> ('a * 'b) conv

    val conv : 'a conv -> 'a -> data Value.desc

    val ( @-> ) :
      'a conv -> 'b conv -> (Warnings.loc -> 'a -> 'b Monad.t) conv
  end
end
