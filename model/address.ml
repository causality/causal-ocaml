type selector =
  | Field of string  (** Field of a record type *)
  | Projection of int  (** [Projection i] is the ith component of a tuple *)
  | Argument of int  (** Argument of a certain call *)
  | Result of int  (** Result of a certain call *)

let show_atom = function
  | Field s -> "." ^ s
  | Projection k -> Printf.sprintf ".(%d)" k
  | Argument k -> Printf.sprintf ".arg(%d)" k
  | Result k -> Printf.sprintf ".result(%d)" k

type t = selector list

let show l =
  match l with
  | [] -> "."
  | l -> String.concat "" @@ List.map show_atom @@ List.rev l

let rec at ((desc, _) as v) path =
  match (desc, path) with
  | _, [] -> Some v
  | Value.Tuple l, Projection k :: rest -> (
    try at (List.nth l k) rest with _ -> None )
  | Value.Record fields, Field s :: rest -> at (List.assoc s fields) rest
  | _, _ -> None

let map ~const ~tuple ~record ~record_item ~constructor ~func =
  let rec aux (addr : t) = function
    | Value.Const c, loc -> const addr c loc
    | Value.Constructor (c, l), loc ->
        constructor addr c (aux_list l addr) loc
    | Value.Tuple l, loc -> tuple addr (aux_list l addr) loc
    | Value.Record r, loc -> record addr (aux_record r addr) loc
    | Value.Function f, loc -> func addr f loc
  and aux_list l (addr : t) =
    List.mapi (fun i v -> aux (addr @ [Projection i]) v) l
  and aux_record record addr =
    List.map
      (fun (name, v) -> record_item addr name (aux (addr @ [Field name]) v))
      record
  in
  aux

let map_value map_f =
  map
    ~const:(fun _ c loc -> (Value.Const c, loc))
    ~constructor:(fun _ c l loc -> (Value.Constructor (c, l), loc))
    ~tuple:(fun _ l loc -> (Value.Tuple l, loc))
    ~record:(fun _ r loc -> (Value.Record r, loc))
    ~record_item:(fun _ name v -> (name, v))
    ~func:(fun addr f loc ->
      (Value.Function {f with data= map_f addr f loc}, loc))

let compare = compare
