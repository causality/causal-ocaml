open Causality

module Make (Monad : Monad.T) = struct
  module Value = MonadicValue.Make (Monad)
  open Monad
  module T = Value.Typ

  let clos x = T.(unit @-> x)

  let succ () = T.(conv (int @-> int) (fun _ n -> return (n + 1)))

  let liftArith2 f () =
    T.(conv (int @-> int @-> int)) (fun _ n ->
        return (fun _ m -> return (f n m)))

  let liftBool2 f () =
    T.(conv (int @-> int @-> bool)) (fun _ n ->
        return (fun _ m -> return (f n m)))

  let liftBBool2 f () =
    T.(conv (bool @-> bool @-> bool)) (fun _ n ->
        return (fun _ m -> return (f n m)))

  let spawn () =
    T.(
      conv
        (clos int @-> clos int)
        (fun loc closure ->
          let c = Location.create () in
          parallel
            [ closure loc () >>= put c >> discard
            ; return (fun _ () -> watch c) ]))

  let var =
    T.(string *** (int @-> unit) *** (unit @-> int) *** (unit @-> int))

  let loc = T.(string *** (int @-> unit) *** (unit @-> int))

  module Cell = Linearisable.Cell (Monad)

  let _create_var n after name =
    let c = Cell.create ~name ~show:Int.to_string n in
    let get loc () =
      let act i = after loc @@ Printf.sprintf "R%s=%d" name i in
      Cell.get ~act c
    in
    let set loc i =
      let act () = after loc @@ Printf.sprintf "W%s=%d" name i in
      Cell.set ~act c i
    in
    let incr loc () =
      let act _ = after loc "Up" in
      Cell.update ~act c (fun n -> (n + 1, n + 1))
    in
    (name, (set, (get, incr)))

  let create_ref after () =
    T.(conv (int @-> var) (fun _ n -> return @@ _create_var n after ""))

  let create_var after () =
    T.(
      conv
        (string @-> int @-> var)
        (fun _ s -> return (fun _ n -> return (_create_var n after s))))

  let get () =
    T.(conv (var @-> int) (fun loc (_, var) -> fst (snd var) loc ()))

  let set () =
    T.(
      conv
        (var @-> int @-> unit)
        (fun loc (_, var) -> return (fun _ k -> fst var loc k)))

  let incr () =
    T.(conv (var @-> int) (fun loc (_, var) -> snd (snd var) loc ()))

  let abort () = T.(conv (unit @-> unit) (fun _ () -> discard))

  let loc_create () =
    T.(
      conv (string @-> loc) (fun _ name ->
          let a = Location.create ~show:Int.to_string ~name () in
          return (name, ((fun _ n -> put a n), fun _ () -> watch a))))

  let loc_create_log after () =
    T.(
      conv (string @-> loc) (fun _ name ->
          let a = Location.create ~show:Int.to_string ~name () in
          return
            ( name
            , ( (fun loc n ->
                  after loc (Printf.sprintf "put(%s, %d)" name n) >> put a n)
              , fun _ () -> watch a ) )))

  let race () =
    let module Channel = Channel.Make (Monad) in
    T.(
      conv
        (((unit @-> int) *** (unit @-> int)) @-> int)
        (fun loc (x, y) ->
          let c = Channel.create () in
          parallel
            [ (x loc () >>= fun x -> Channel.send c x >> discard)
            ; y loc () >>= Channel.send c >> discard
            ; (Channel.recv c >>= fun n -> return n) ]))

  let builtins after =
    [ ("succ", succ)
    ; ("+", liftArith2 ( + ))
    ; ("-", liftArith2 ( - ))
    ; ("*", liftArith2 ( * ))
    ; ("/", liftArith2 ( / ))
    ; ("<=", liftBool2 ( <= ))
    ; ("<", liftBool2 ( < ))
    ; ("=", liftBool2 ( = ))
    ; (">", liftBool2 ( > ))
    ; (">=", liftBool2 ( >= ))
    ; ("&&", liftBBool2 ( && ))
    ; ("||", liftBBool2 ( || ))
    ; ("spawn", spawn)
    ; ("abort", abort)
    ; ("var", create_var after)
    ; ("incr", incr)
    ; ("ref", create_ref after)
    ; ("create", loc_create)
    ; ("create_log", loc_create_log after)
    ; (":=", set)
    ; ("!", get)
    ; ("put", set)
    ; ("race", race)
    ; ("watch", get) ]

  let init =
    Ocaml.Term.env_of_signature
      "val ( + ) : int -> int -> int\n\n\
       val ( - ) : int -> int -> int\n\n\
       val ( * ) : int -> int -> int\n\n\
       val ( / ) : int -> int -> int\n\n\
       val ( <= ) : int -> int -> bool\n\n\
       val ( < ) : int -> int -> bool\n\n\
       val ( = ) : int -> int -> bool\n\n\
       val ( > ) : int -> int -> bool\n\n\
       val ( >= ) : int -> int -> bool\n\n\
       val ( && ) : bool -> bool -> bool\n\n\
       val ( || ) : bool -> bool -> bool\n\n\
       val succ : int -> int\n\n\
       val spawn : (unit -> 'a) -> (unit -> 'a)\n\n\
       val abort : unit -> 'a\n\n\
      \       type var\n\n\
      \       val var : string -> int -> var\n\
      \       val ref : int -> var\n\
      \       val ( ! ) : var -> int\n\
      \       val incr : var -> int\n\
      \       val ( := ) : var -> int -> unit\n\n\n\n\
      \       type loc\n\
       val race : (unit -> int) * (unit -> int) -> int\n\
       val create : string -> loc\n\
       val create_log : string -> loc\n\
       val watch : loc -> int\n\
       val put :  loc -> int -> unit"
end
