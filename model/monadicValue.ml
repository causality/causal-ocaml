module Make (Monad : Monad.T) = struct
  type data = Func of (Ocaml.loc -> t -> t Monad.t)

  and desc = data Value.desc

  and t = data Value.t

  type venv = (string * t) list

  type benv = (string * (unit -> desc)) list

  let show_function func =
    Printf.sprintf "λ%s:%s"
      (Option.value
         (Option.map Ocaml.Pattern.show func.Value.pat)
         ~default:"")
    @@ Ocaml.Typ.(show (func.Value.src --> func.Value.tgt))

  let to_string x = Value.to_string show_function x

  let to_string_kind x = Value.to_string_desc show_function x

  let apply ?(loc = Location.none) ~arg = function
    | Value.Function {data= Func exec; _}, _ -> exec loc arg
    | _ -> failwith "Value.apply: Not a function"

  let _function ~src ~tgt ?name ?pat f =
    Value._function ~src ~tgt ?name ?pat (Func f)

  module Typ = struct
    module T = Value.Typ (struct
      type nonrec data = data
    end)

    include T

    let ( @-> ) source target =
      let open Monad in
      { ocaml= Ocaml.Typ.(source.ocaml --> target.ocaml)
      ; from_val=
          (function
          | Function {data= Func f; _}, loc' ->
              Some
                (fun loc x ->
                  bind
                    (f loc (source.to_val x, loc'))
                    (fun x ->
                      match target.from_val x with
                      | Some s -> return s
                      | None -> discard))
          | _ -> None)
      ; to_val=
          (fun f ->
            _function ~src:source.ocaml ~tgt:target.ocaml (fun loc v ->
                match source.from_val v with
                | Some x ->
                    bind (f loc x) (fun x -> return (target.to_val x, snd v))
                | None -> discard)) }
  end
end
