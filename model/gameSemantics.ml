module Make (Monad : GameModel.GamingMonad) = struct
  module SemValue = MonadicValue.Make (Monad)
  module L = GameModel.Label

  let _closure_name =
    let r = ref 0 in
    fun () -> Printf.sprintf "lambda%d" (incr r ; !r)

  let of_value = Value.map (fun _ -> ())

  open Monad

  let rec make_closure player pointer address tgt =
    let run _loc x =
      play ~pointer player address {kind= Call tgt; payload= of_value x}
      >>= fun move ->
      parallel
        [ send_after ~pointer:move player
            ~address:[Address.Argument move.GameModel.Label.id]
            x
          >> discard
        ; receive player ~pointer:move ~address:[Address.Result move.id] ]
    in
    SemValue.Func run

  and receive ?(address = []) ?pointer player =
    wait ?pointer address >>= receive_after player

  and receive_after ?(address = []) player pointer =
    let convert addr {Value.tgt; _} _ =
      make_closure player pointer addr tgt
    in
    return (Address.map_value convert address (L.data pointer).payload)

  and send ?pointer ?(address = []) player value =
    let payload = of_value value in
    play ?pointer player address {kind= Return; payload}
    >>= fun label -> send_after player ~pointer:label value

  and send_after player ~pointer ?(address = []) value =
    Address.map
      ~const:(fun _ _ _ -> return ())
      ~constructor:(fun _ _ l _ -> parallel l)
      ~tuple:(fun _ l _ -> parallel l)
      ~record:(fun _ l _ -> parallel l)
      ~record_item:(fun _ _ v -> v)
      ~func:(fun address {data= SemValue.Func f; _} loc ->
        wait ~pointer address
        >>= fun call ->
        match (L.data call).kind with
        | Return -> discard (* expecting a call *)
        | Call _ ->
            receive_after player ~address:[Argument call.id] call
            >>= fun v ->
            f loc v
            >>= fun result ->
            send ~pointer:call player ~address:[Result call.id] result)
      address value

  module Interpreter = Interpreter.Make (Monad)

  let expression builtins player exp =
    Interpreter.expression builtins [] exp
    >>= fun result -> send player result

  let structure builtins player exp =
    Interpreter.structure builtins [] exp
    >>= fun result -> send player result
end
