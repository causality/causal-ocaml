module type T = sig
  type 'a t

  val bind : 'a t -> ('a -> 'b t) -> 'b t

  val return : 'a -> 'a t

  val discard : 'a t

  val join_list : 'a t list -> 'a list t
end
