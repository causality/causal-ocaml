module Payload = struct
  type t = unit Value.t

  type desc = unit Value.desc

  let show_closure {Value.name; _} = if name = "" then "{Function}" else name

  let show x = Value.to_string show_closure x

  let show_desc = Value.to_string_desc show_closure
end

module Signature = struct
  type address = Address.t

  type kind = Call of Ocaml.Typ.t | Return

  type visible = {kind: kind; payload: Payload.t}

  type neutral = {str: string; loc: Location.t}

  module Player = struct
    type t = Program | Context

    let compare = compare
  end
end

include Causality.Game.Make (Signature)

module Show = struct
  open Signature.Player

  let concat sep l =
    let rec aux = function
      | [] -> []
      | [t] -> [t]
      | t :: q -> t :: sep () :: aux q
    in
    List.concat @@ aux (List.filter (fun x -> x <> []) l)

  let show_move _empty txt button make_return make_call move =
    let argument_name =
      match Label.pointer move with
      | None -> ""
      | Some m -> (
          let address = Label.address move in
          let address =
            match (Label.data m).kind with
            | Call _ -> ( try List.tl address with _ -> address )
            | _ -> address
          in
          match Address.at (Label.data m).payload address with
          | Some (Value.Function {name; _}, _) -> name
          | _ -> "" )
    in
    let kind =
      match (Label.data move).kind with
      | Signature.Call tgt when Label.player move = Program ->
          [ txt @@ Printf.sprintf "Call "
          ; button argument_name "Return from this program call"
              (make_return move.id tgt move)
          ; txt " with " ]
      | Call _ -> [txt @@ Printf.sprintf "Call %s with " argument_name]
      | _ -> [txt "Return: "]
    in
    let init_address =
      match (Label.data move).kind with
      | Call _ -> [Address.Argument move.id]
      | _ -> []
    in
    let concat init stop sep l =
      (txt init :: concat (fun () -> [txt sep]) l) @ [txt stop]
    in
    let vtype =
      Address.map
        ~const:(fun _ c _ -> [txt @@ Printpat.pretty_const c])
        ~func:(fun address func _ ->
          let text =
            Printf.sprintf "%s: %s"
              (if func.name = "" then "fun" else func.name)
              Ocaml.Typ.(show (func.src --> func.tgt))
          in
          if Label.player move = Program then
            [ button text "Call this program function"
                (make_call func move address) ]
          else [txt text])
        ~tuple:(fun _ l _ -> concat "(" ")" "," l)
        ~record:(fun _ l _ -> concat "{" "}" "; " l)
        ~record_item:(fun _ name x ->
          (txt @@ Printf.sprintf "%s = " name) :: x)
        ~constructor:(fun _ c l _ ->
          match l with [] -> [txt c] | _ -> txt c :: concat " (" ")" ", " l)
    in
    kind @ vtype init_address (Label.data move).payload

  let to_document empty txt button make_return make_call event =
    match event.Label.desc with
    | `Visible move ->
        show_move empty txt button make_return make_call
          {event with desc= `Visible move}
    | `Neutral {Signature.str; _} -> [txt str]

  let to_string e =
    String.concat ""
    @@ to_document ""
         (fun s -> s)
         (fun s _ _ -> s)
         (fun _ _ _ _ -> ())
         (fun _ _ _ _ -> ())
         e
end

module Monad = Monad (Show)
