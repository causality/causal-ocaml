(** Definition of the game model *)

module Payload : sig
  type t = unit Value.t

  type desc = unit Value.desc

  val show : t -> string

  val show_desc : desc -> string
end

module Signature : sig
  type address = Address.t

  type neutral = {str: string; loc: Location.t}

  type kind = Call of Ocaml.Typ.t | Return

  type visible = {kind: kind; payload: Payload.t}

  module Player : sig
    type t = Program | Context

    val compare : 'a -> 'a -> int
  end
end

include module type of Causality.Game.Make (Signature)

module Show : sig
  val to_string : Label.t -> string

  val to_document :
       'a
    -> (string -> 'b)
    -> (string -> string -> 'c -> 'b)
    -> (int -> Types.type_expr -> Label.visible -> 'c)
    -> (unit Value.func -> Label.visible -> Address.t -> 'c)
    -> [< `Neutral of Signature.neutral | `Visible of Label.visible_desc]
       Label._t
    -> 'b list
end

module Monad : module type of Monad (Show)
