(** Game Semantics of OCaml values.

    This module defines how to convert to send and receive OCaml values on
    the network over locations exchanging messages. It is parametrised over a
    gaming monad (see Causality.Game for a definition *)

module Make (Monad : GameModel.GamingMonad) : sig
  module SemValue := MonadicValue.Make(Monad)

  val receive :
       ?address:Address.t
    -> ?pointer:GameModel.Label.visible
    -> GameModel.Signature.Player.t
    -> SemValue.t Monad.t
  (** Receive a value from the network at address [address], with pointer
      [pointer]. *)

  val send :
       ?pointer:GameModel.Label.visible
    -> ?address:Address.t
    -> GameModel.Signature.Player.t
    -> SemValue.data Value.t
    -> unit Monad.t
  (** Send a value to the network at address [address], with pointer
      [pointer]. *)

  val expression :
       SemValue.benv
    -> GameModel.Signature.Player.t
    -> Typedtree.expression
    -> unit Monad.t

  (** Given an expression, perform the monadic translation. *)
  val structure :
       SemValue.benv
    -> GameModel.Signature.Player.t
    -> Typedtree.structure
    -> unit Monad.t
end
