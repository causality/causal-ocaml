type loc = Location.t

module Typ : sig
  type t = Types.type_expr

  val ( --> ) : t -> t -> t

  val show : t -> string

  val builtin : string -> t

  val int : t

  val bool : t

  val var : t

  val string : t

  val unit : t

  val ( *** ) : t -> t -> t

  val decompose_arrow : t -> (t * t) option
end

module Term : sig
  val loc_of_error : exn -> Location.t option

  val report_error : exn -> string * Location.t option

  val signature : string -> Typedtree.signature

  val handle :
    ?on_error:(string -> Location.t option -> unit) -> (unit -> unit) -> unit

  val structure :
    ?prelude:string -> Env.t -> string -> Typedtree.structure * Env.t

  val env_of_signature : string -> Env.t

  val expression : Env.t -> string -> Typedtree.expression

  val typ_of_struct : Typedtree.structure -> Types.type_expr
end

module Pattern : sig
  type t = Typedtree.pattern

  val show : t -> string
end

val string_of_loc : Location.t -> string
