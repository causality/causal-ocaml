module Make (Monad : Monad.T) : sig
  module SValue : module type of MonadicValue.Make (Monad)

  val expression :
    SValue.benv -> SValue.venv -> Typedtree.expression -> SValue.t Monad.t

  val structure :
    SValue.benv -> SValue.venv -> Typedtree.structure -> SValue.t Monad.t
end
