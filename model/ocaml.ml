type loc = Location.t

let fmt_to_string f =
  let b = Buffer.create 512 in
  let fmt = Format.formatter_of_buffer b in
  f fmt ; Format.fprintf fmt "%!" ; Buffer.contents b

module Typ = struct
  type t = Types.type_expr

  let ( --> ) a b = Ctype.newty Types.(Tarrow (Nolabel, a, b, Cok))

  let show t = fmt_to_string (fun fmt -> Printtyp.type_sch fmt t)

  let builtin name =
    Ctype.newconstr (Path.Pident (Ident.create_predef name)) []

  let int = builtin "int"

  let bool = builtin "bool"

  let var = builtin "var"

  let string = builtin "string"

  let unit = builtin "unit"

  let ( *** ) t1 t2 = Ctype.newty Types.(Ttuple [t1; t2])

  let rec decompose_arrow t =
    match (Ctype.repr t).desc with
    | Types.Tarrow (_, a, b, _) -> Some (a, b)
    | Tlink t -> decompose_arrow t
    | Tsubst t -> decompose_arrow t
    | _ -> None
end

module Term = struct
  let loc_of_error = function
    | Typetexp.Error (loc, _, _) -> Some loc
    | Typecore.Error (loc, _, _) -> Some loc
    | Syntaxerr.Error err -> Some (Syntaxerr.location_of_error err)
    | Lexer.Error (_, loc) -> Some loc
    | _ -> None

  let report_error exn =
    match loc_of_error exn with
    | None -> (Printexc.to_string exn, None)
    | Some loc ->
        ( fmt_to_string (fun fmt -> Location.report_exception fmt exn)
        , Some loc )

  let handle ?(on_error = fun _ _ -> ()) f =
    Printexc.record_backtrace true ;
    try f ()
    with
    | Env.Error e ->
       Env.report_error Format.std_formatter e
    | exn ->
      let s, loc = report_error exn in
      Printf.printf "*** Exception: %s\n" s ;
      Printexc.print_backtrace stdout ;
      on_error s loc

  let signature string =
    let ptree = Parse.interface (Lexing.from_string string) in
    Typemod.type_interface Env.initial_safe_string ptree

  let structure ?(prelude = "") env string =
    let filter x =
      match x.Parsetree.pstr_desc with
      | Parsetree.Pstr_open _ -> false
      | _ -> true
    in
    let ptree =
      List.filter filter
      @@ Parse.implementation (Lexing.from_string (prelude ^ string))
    in
    let x, _, _, env = Typemod.type_structure env ptree Location.none in
    (x, env)

  let env_of_signature s = (signature s).Typedtree.sig_final_env

  let expression env string =
    let ptree = Parse.expression (Lexing.from_string string) in
    Typecore.type_exp env ptree

  let typ_of_struct s =
    match (List.hd s.Typedtree.str_items).str_desc with
    | Typedtree.Tstr_eval (x, _) -> x.exp_type
    | _ -> failwith "Not an eval"
end

let string_of_loc loc = fmt_to_string (fun fmt -> Location.print_loc fmt loc)

module Pattern = struct
  type t = Typedtree.pattern

  let show pat = fmt_to_string (fun fmt -> Printpat.pretty_line fmt [pat])
end
