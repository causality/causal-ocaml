type 'data func =
  { src: Ocaml.Typ.t  (** Source type *)
  ; tgt: Ocaml.Typ.t  (** Target type *)
  ; pat: Ocaml.Pattern.t option  (** Pattern binding the variable *)
  ; name: string  (** Name of the function (maybe be empty) *)
  ; data: 'data  (** Actual function *) }

type 'data desc =
  | Const of Asttypes.constant  (** Constant, eg. int, string, ... *)
  | Function of 'data func  (** Function *)
  | Record of (string * 'data t) list  (** Record type *)
  | Tuple of 'data t list  (** Tuple *)
  | Constructor of string * 'data t list  (** Constructor of a sum type *)

(** A value is a pair of a location where the value is defined and its
    description *)
and 'data t = 'data desc * Location.t

let _function ~src ~tgt ?(name = "") ?pat data =
  Function {src; pat; tgt; name; data}

let rec map f (x, loc) = (map_desc f x, loc)

and map_desc f = function
  | Const c -> Const c
  | Function x -> Function {x with data= f x.data}
  | Tuple l -> Tuple (List.map (map f) l)
  | Constructor (s, l) -> Constructor (s, List.map (map f) l)
  | Record l -> Record (List.map (fun (name, v) -> (name, map f v)) l)

let rec to_string_desc show_func = function
  | Const c -> Printpat.pretty_const c
  | Function f -> show_func f
  | Tuple l ->
      Printf.sprintf "(%s)"
        (String.concat ", " (List.map (to_string show_func) l))
  | Record l ->
      "{"
      ^ String.concat "; "
          (List.map
             (fun (name, v) -> name ^ "=" ^ to_string_desc show_func (fst v))
             l)
      ^ "}"
  | Constructor (name, []) -> name
  | Constructor (name, l) ->
      Printf.sprintf "%s (%s)" name
        (String.concat ", " @@ List.map (to_string show_func) l)

and to_string f (v, _) = to_string_desc f v

let rec of_expression e =
  let open Typedtree in
  let loc = e.exp_loc in
  match e.exp_desc with
  | Texp_constant c -> (Const c, loc)
  | Texp_construct (_, c, args) ->
      (Constructor (c.cstr_name, List.map of_expression args), loc)
  | Texp_tuple l -> (Tuple (List.map of_expression l), loc)
  | Texp_record record ->
      ( Record
          ( List.map (function
              | a, Overridden (_, e) -> (a.Types.lbl_name, of_expression e)
              | _ -> failwith "Invalid record")
          @@ Array.to_list record.fields )
      , loc )
  | _ -> failwith "Invalid value"

let of_string env s =
  let e = Ocaml.Term.expression env s in
  of_expression e

let ttrue = Constructor ("true", [])

let ffalse = Constructor ("false", [])

let int n = Const (Const_int n)

let string s = Const (Const_string (s, Location.none, None))

let unit = Constructor ("()", [])

let bool b = if b then ttrue else ffalse

module Typ (T : sig
  type data
end) =
struct
  type 'a conv =
    { ocaml: Ocaml.Typ.t
    ; from_val: T.data t -> 'a option
    ; to_val: 'a -> T.data desc }

  let int =
    { ocaml= Ocaml.Typ.int
    ; from_val= (function Const (Const_int n), _ -> Some n | _ -> None)
    ; to_val= int }

  let string =
    { ocaml= Ocaml.Typ.string
    ; from_val=
        (function Const (Const_string (s, _, _)), _ -> Some s | _ -> None)
    ; to_val= string }

  let unit =
    { ocaml= Ocaml.Typ.unit
    ; from_val= (fun _ -> Some ())
    ; to_val= (fun () -> unit) }

  let ( *** ) t1 t2 =
    { ocaml= Ocaml.Typ.(t1.ocaml *** t2.ocaml)
    ; from_val=
        (function
        | Tuple [a; b], _ -> (
          match (t1.from_val a, t2.from_val b) with
          | Some x, Some y -> Some (x, y)
          | _ -> None )
        | _ -> None)
    ; to_val=
        (fun (x, y) ->
          Tuple [(t1.to_val x, Location.none); (t2.to_val y, Location.none)])
    }

  let bool =
    { ocaml= Ocaml.Typ.bool
    ; from_val=
        (function
        | Constructor ("true", []), _ -> Some true
        | Constructor ("false", []), _ -> Some false
        | _ -> None)
    ; to_val= bool }

  let conv typ x = typ.to_val x
end
