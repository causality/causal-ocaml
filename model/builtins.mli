module Make (M : Causality.Monad.T) : sig
  val builtins :
    (Warnings.loc -> string -> unit M.t) -> MonadicValue.Make(M).benv

  val init : Env.t
end
