(** Values - representation of OCaml values. *)

(** In this module, we define values which are representation of the result
    of the evaluation of a term. What we define here is parametrised over the
    choice of the representation of functions, which will be instantiated in
    two ways: in {!monadicValues} as Kleisli maps, or in {!Move} as nothing. *)

(** Representation of a function, parametrised over a choice of
    representation of the actual functions. Other fields are just metadata. *)
type 'data func =
  { src: Ocaml.Typ.t  (** Source type *)
  ; tgt: Ocaml.Typ.t  (** Target type *)
  ; pat: Ocaml.Pattern.t option  (** Pattern binding the argument *)
  ; name: string  (** Name of the function (maybe be empty) *)
  ; data: 'data  (** Actual function *) }

(** Description of a value *)
type 'data desc =
  | Const of Asttypes.constant  (** Constant, eg. int, string, ... *)
  | Function of 'data func  (** Function *)
  | Record of (string * 'data t) list  (** Record type *)
  | Tuple of 'data t list  (** Tuple *)
  | Constructor of string * 'data t list  (** Constructor of a sum type *)

(** A value is a pair of a location where the value is defined and its
    description *)
and 'data t = 'data desc * Location.t

val map_desc : ('a -> 'b) -> 'a desc -> 'b desc
(** Convert descriptions between different function representation *)

val map : ('a -> 'b) -> 'a t -> 'b t
(** Convert values between different function representation *)

(** {1 Some values} *)

val ttrue : 'closure desc

val ffalse : 'closure desc

val int : int -> 'closure desc

val string : string -> 'closure desc

val unit : 'closure desc

val bool : bool -> 'closure desc

val _function :
     src:Types.type_expr
  -> tgt:Types.type_expr
  -> ?name:string
  -> ?pat:Ocaml.Pattern.t
  -> 'a
  -> 'a desc
(** {1 String conversion} *)

val to_string : ('data func -> string) -> 'data t -> string

val to_string_desc : ('data func -> string) -> 'data desc -> string

val of_string : Env.t -> string -> 'data t
(** parse a value -- only works for base type values *)

module Typ (S : sig
  type data
end) : sig
  type 'a conv =
    { ocaml: Ocaml.Typ.t
    ; from_val: S.data t -> 'a option
    ; to_val: 'a -> S.data desc }

  val int : int conv

  val string : string conv

  val unit : unit conv

  val bool : bool conv

  val ( *** ) : 'a conv -> 'b conv -> ('a * 'b) conv

  val conv : 'a conv -> 'a -> S.data desc
end
